# Tic Tac Toe
# Copyright (C) 2019 NecroGeeks
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

extends Node2D

const Player = preload("res://src/Player.gd")

onready var Player1 = Player.new("X")
onready var Player2 = Player.new("O")
onready var Controller : Player

func _ready():
	$Board.set_board_controller(Player1)
	$Board.hide()

func _on_Board_played_turn():
	var controller = $Board.get_board_controller()
	if controller.ID == Player1.ID:
		$Board.set_board_controller(Player2)
	else:
		$Board.set_board_controller(Player1)
	print("Player {0} played.".replace('{0}', controller.ID))

func _on_Board_tied_game():
	$GameEndScreen.set_text("Tied game!")
	$GameEndScreen.show()
	print("Game was tied.")

func _on_Board_won_game(winner: Player):
	$GameEndScreen.set_text(winner.ID + " won!")
	$GameEndScreen.show()
	print("Player {0} won.".replace('{0}', winner.ID))

func _on_TitleScreen_selected_multi_player():
	$TitleScreen.hide()
	$Board.show()

func _on_TitleScreen_selected_single_player():
	$TitleScreen.hide()
	$Board.show()

func _on_GameEndScreen_reset_game():
	$Board.reset_board()
	$GameEndScreen.hide()
