# Tic Tac Toe
# Copyright (C) 2019 NecroGeeks
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

extends Node2D

const Player = preload("res://src/Player.gd")

signal won_game(winner)
signal tied_game
signal played_turn

var _controller : Player
var _board_check_method = [
	# rows
	[0,1,2],[3,4,5],[6,7,8],
	# columns
	[0,3,6],[1,4,7],[2,5,8],
	# diagnals
	[0,4,8],[6,4,2]
]

func _ready():
	# Setting the pressed event on all the buttons on the board
	for i in $Buttons.get_child_count():
		var button = $Buttons.get_child(i)
		button.connect("pressed", self, "_pressed_button", [button])

func _pressed_button(button):
	# simple check to not override a button that has already
	# been pressed
	if button.text != "":
		return false
		
	button.text = _controller.ID
	if check_board_for_win():
		disable_board()
		return emit_signal("won_game", _controller)
	elif check_board_for_tie():
		disable_board()
		return emit_signal("tied_game")
	emit_signal("played_turn")

func set_board_controller(controller: Player):
	_controller = controller

func get_board_controller() -> Player:
	return _controller

func reset_board():
	for i in $Buttons.get_child_count():
		$Buttons.get_child(i).text = ""
		$Buttons.get_child(i).disabled = false

func disable_board():
	for i in $Buttons.get_child_count():
		$Buttons.get_child(i).disabled = true
	
func check_board_for_win():
	var board = $Buttons.get_children()
	for section in _board_check_method:
		var button1 = board[section[0]].text
		var button2 = board[section[1]].text
		var button3 = board[section[2]].text
		
		if button1 == "" and button2 == "" and button3 == "":
			continue
		if button1 == button2 and button2 == button3:
			return true	
	return false
	
func check_board_for_tie():
	var board = $Buttons.get_children()
	var count = 0
	for button in board:
		if button.text != "":
			count += 1
	if count == 9:
		return true
	return false
	
func show():
	reset_board()
	.show()
	
func hide():
	reset_board()
	.hide()