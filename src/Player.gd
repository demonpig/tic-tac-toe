# Tic Tac Toe
# Copyright (C) 2019 NecroGeeks
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

var ID = "" setget ,get_id
var _is_cpu = false

func _init(player_id):
	ID = str(player_id)
	
func get_id():
	return ID
	
func is_cpu():
	return _is_cpu
	
func set_as_cpu():
	_is_cpu = true