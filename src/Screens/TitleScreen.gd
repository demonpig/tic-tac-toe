# Tic Tac Toe
# Copyright (C) 2019 NecroGeeks
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

extends Control

signal selected_single_player()
signal selected_multi_player()

func _on_SinglePlayerButton_pressed():
	emit_signal("selected_single_player")

func _on_MultiplayerButton_pressed():
	emit_signal("selected_multi_player")
