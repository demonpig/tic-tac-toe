#
# spec file for package com.gitlab.necrogeeks.tic-tac-toe
#


%define generic_name tic-tac-toe

Name:           com.gitlab.necrogeeks.tic-tac-toe
Version:        v0.2.1
Release:        0
Summary:        Simple game of X and O matching.
License:        GPL-3.0-or-later
Group:          Amusements/Games/Board/Other
URL:            https://gitlab.com/necrogeeks/%{generic_name}
Source0:        https://gitlab.com/necrogeeks/%{generic_name}/-/archive/%{version}/%{generic_name}-%{version}.tar.gz
BuildRequires:  godot-headless
BuildRequires:  ImageMagick
Requires:       godot-runner
Requires:       update-desktop-files
Requires(post): update-desktop-files
Requires(postun): update-desktop-files
Provides:       %{generic_name} = %{version}

%description
Simple game of X and O matching.

%prep
%setup -q -n %{generic_name}-%{version}

%build
/usr/bin/godot-headless -v --path ./ --export "Linux" linux.pck

%install
install -D -p -m 644 linux.pck %{buildroot}%{_datadir}/%{name}/main.pck
install -D -p -m 644 distribution/linux/%{name}.appdata.xml %{buildroot}%{_datadir}/share/appdata/%{name}.appdata.xml

for res in 256 128 48 32 24 16; do
    install -d %{buildroot}%{_datadir}/icons/hicolor/$res"x"$res/apps
    convert -strip distribution/icon.png \
            -resize "$res"x"$res" \
            -quality 100 \
            %{buildroot}%{_datadir}/icons/hicolor/$res'x'$res/apps/%{name}.png
done

mkdir -p %{buildroot}%{_bindir}
cat > %{buildroot}%{_bindir}/%{name} <<EOF
#!/bin/bash
/usr/bin/godot-runner --main-pack %{_datadir}/%{name}/main.pck
EOF
chmod 0755 %{buildroot}%{_bindir}/%{name}

#                            filename name            generic-name    exec    icon    category 
%suse_update_desktop_file -c %{name}  %{generic_name} %{generic_name} %{name} %{name} "Game;BoardGame;"

%files
%license LICENSE
%dir %{_datadir}/%{name}
%dir %{_datadir}/icons/hicolor
%dir %{_datadir}/icons/hicolor/16x16
%dir %{_datadir}/icons/hicolor/16x16/apps
%dir %{_datadir}/icons/hicolor/24x24
%dir %{_datadir}/icons/hicolor/24x24/apps
%dir %{_datadir}/icons/hicolor/32x32
%dir %{_datadir}/icons/hicolor/32x32/apps
%dir %{_datadir}/icons/hicolor/48x48
%dir %{_datadir}/icons/hicolor/48x48/apps
%dir %{_datadir}/icons/hicolor/128x128
%dir %{_datadir}/icons/hicolor/128x128/apps
%dir %{_datadir}/icons/hicolor/256x256
%dir %{_datadir}/icons/hicolor/256x256/apps
%dir %{_datadir}/share/appdata
%{_bindir}/%{name}
%{_datadir}/%{name}/main.pck
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/16x16/apps/%{name}.png
%{_datadir}/icons/hicolor/24x24/apps/%{name}.png
%{_datadir}/icons/hicolor/32x32/apps/%{name}.png
%{_datadir}/icons/hicolor/48x48/apps/%{name}.png
%{_datadir}/icons/hicolor/128x128/apps/%{name}.png
%{_datadir}/icons/hicolor/256x256/apps/%{name}.png
%{_datadir}/share/appdata/%{name}.appdata.xml

%changelog
* Sun Oct 06 2019 Max Mitschke <maxmitschke@fastmail.com> - v0.2.1
- Initial packaging for game